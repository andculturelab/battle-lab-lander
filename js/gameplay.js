
$(function() {
  $("#tabs").tabs({
    show: { effect: "pulsate", direction: "right", duration: 300 }
  });
  $("#accordion").accordion();

  var btn = $("#accordion li a");
  var wrapper = $("#accordion li");

  $(btn).on("click", function() {
    $(btn).removeClass("active");
    $(btn)
      .parent()
      .find(".addon")
      .removeClass("fadein");

    $(this).addClass("active");
    $(this)
      .parent()
      .find(".addon")
      .addClass("fadein");
  });
});

$( function() {
    $( ".glitch-img" ).mgGlitch({
          // set 'true' to stop the plugin
          destroy : false, 
          // set 'false' to stop glitching
          glitch: true, 
          // set 'false' to stop scaling
          scale: true, 
          // set 'false' to stop glitch blending
          blend : true, 
          // select blend mode type
          blendModeType : 'hue',
          // set min time for glitch 1 elem
          glitch1TimeMin : 1000, 
          // set max time for glitch 1 elem
          glitch1TimeMax : 6000,
          // set min time for glitch 2 elem
          glitch2TimeMin : 100, 
          // set max time for glitch 2 elem
          glitch2TimeMax : 400, 
    });
});

$(document).ready(function() {
  $('[data-modal]').on('click', function(e) {
    classname = "myClass";
    $target = $(e.currentTarget);
    $('[data-modal-target="' + $target.data('modal') + '"]').show();
  });
  $('[data-modal-target] .close').on('click', function(e) {
    $target = $(e.currentTarget);
    $target.parents('[data-modal-target]').hide();
  });
});



